<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
	protected $fillable = [
		'user_id', 'name', 'start_date', 'finish_date', 
    ];

    public function users(){
    	return $this->belongsTo('App\User');
    }
}

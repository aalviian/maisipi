<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
	protected $fillable = [
		'user_id', 'name', 'year', 'certificate', 
    ];

    public function users(){
    	return $this->belongsTo('App\User');
    }
}

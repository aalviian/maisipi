<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = [
    	'user_id', 'name', 'percentage',
    ];

    public function users(){
    	return $this->belongsTo('App\User');
    }
}

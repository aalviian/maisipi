<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
	protected $fillable = [
    	'user_id', 'name', 'year', 'description',
    ];

    public function users(){
    	return $this->belongsTo('App\User');
    }
}

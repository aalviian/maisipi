<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
	//protected $table = 'portfolios';

    protected $fillable = [
    	'user_id', 'name', 'description', 'cover',
    ];

    public function users(){
    	return $this->belongsTo('App\User');
    }
}

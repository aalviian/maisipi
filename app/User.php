<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'surname', 'email', 'username', 'password', 'location', 'about_me', 'birth_date', 'address', 'religion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password){
        $this->attributes['password'] = \Hash::make($password);
    }

    public function educations(){
        return $this->hasMany('App\Education');
    }

    public function portfolios(){
        return $this->hasMany('App\Portfolio');
    }

    public function organizations(){
        return $this->hasMany('App\Organization');
    }

    public function experiences(){
        return $this->hasMany('App\Experience');
    }

    public function skills(){
        return $this->hasMany('App\Skill');
    }

    public function achievements(){
        return $this->hasMany('App\Achievement');
    }
}
 
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
	protected $fillable = [
		'user_id', 'name', 'start_date', 'finish_date','position', 
    ];

    public function users(){
    	return $this->belongsTo('App\User');
    }
}

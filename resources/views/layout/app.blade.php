<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>My Journey Never End | @yield('title')</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" href="{{ asset('css/ion/ion.rangeSlider.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/ion/ion.rangeSlider.skinFlat.css') }}"/>
        <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('css/theme-default.css') }}"/>
        <!-- EOF CSS INCLUDE -->                   
    </head>

    <body class="page-container-boxed">

        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START SIDE NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo"> 
                        <a href="{{ url('/profile') }}">Future Closer</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{ asset('assets/images/users/newava.jpg') }}" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{ asset('assets/images/users/newava.jpg') }}" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">Anastiara Adina Restu</div>
                                <div class="profile-data-title">Student of IPB</div>
                            </div>
                            <div class="profile-controls">
                                <a href="{{ url('/profile') }}" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="{{ url('/profile') }}" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title"></li>
                    <li class="">
                        <a href="{{ url('/profile') }}"><span class="fa fa-dashboard"></span> <span class="xn-text">Profile</span></a>
                    </li> 
                    <li class="">
                        <a href="{{ url('/educations-portfolios') }}" class="active"><span class="fa fa-dashboard"></span> <span class="xn-text">Educations & Portfolios</span></a>
                    </li>
                    <li class="">
                        <a href="{{ url('/organizations-experiences') }}"><span class="fa fa-dashboard"></span> <span class="xn-text">Organizations & Experiences</span></a>
                    </li>  
                    <li class="">
                        <a href="{{ url('/skills-achievements') }}"><span class="fa fa-dashboard"></span> <span class="xn-text">Skills & Achievements</span></a>
                    </li> 
                </ul>
                <!-- END SIDE NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
@yield('content')
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE LOGOUT -->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if you want to continue work. Press Yes to logout out of system 7</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE LOGOUT-->
        
        <!-- MODAL PASSWORD -->
        <div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Change password</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-body form-horizontal form-group-separated">                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Old Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="old_password" name="old_password"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">New Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="new_password" name="new_password"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Confirmation New Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="re_password" name="re_password"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="postPass" class="btn btn-danger" data-dismiss="modal">Proccess</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>        
        <!-- END MODAL PASSWORD -->
        

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{ asset('audio/alert.mp3') }}" preload="auto"></audio>
        <audio id="audio-fail" src="{{ asset('audio/fail.mp3') }}" preload="auto"></audio>
        <!-- END PRELOADS -->                 
     

        <!-- START SCRIPTS -->
        <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery-ui.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>   
        <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery-migrate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/form/jquery.form.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/cropper/cropper.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/fileinput/fileinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/rangeslider/jQAllRangeSliders-min.js') }}"></script>  
        <script type="text/javascript" src="{{ asset('js/plugins/knob/jquery.knob.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/plugins/ion/ion.rangeSlider.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>        
        <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>
@yield('script')<!-- END SCRIPTS -->

    </body>
</html>







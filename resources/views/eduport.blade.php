@extends('layout.app')
@section('title') Educations - Portfolios @endsection                 
             
            @section('content') 
            <!-- START PAGE CONTENT -->
            <div class="page-content">
            <!-- LOCK SCREEEN AND SIGN OUT BUTTON -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button">
                    <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li>
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
            </ul>
            <!-- END LOCK SCREEEN AND SIGN OUT BUTTON -->                  
 
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Educations-Portfolios</a></li>
                <li class="active">Edit</li>
            </ul>
            <!-- END BREADCRUMB -->                 

            <!-- START EDIT EDICATION-PORTFOLIO PAGE -->
            <div class="page-title">                    
                <h2><span class="fa fa-briefcase"></span> Educations-Portfolios</h2>
            </div>                   
            <div class="page-content-wrap">
                <div class="panel panel-default tabs">
                    <!-- NAV TAB -->
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab8" data-toggle="tab">Education</a></li>
                        <li><a href="#tab9" data-toggle="tab">Portfolio</a></li>
                    </ul>
                    <!-- END NAV TAB -->
                    <!-- TAB CONTENT -->
                    <div class="panel-body tab-content">
                        <!-- EDUCATION TAB -->
                        <div class="tab-pane active" id="tab8">
                            <div class="row">  
                                <form enctype="multipart/form-data" class="form-horizontal">
                                    <div class="col-md-9">
                                        <div class="timeline timeline-right">
                                            <!-- SD -->
                                            <div class="timeline-item timeline-main">
                                                <div class="timeline-date">SD</div>
                                            </div>
                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon"><span class="fa fa-info-circle"></span></div>                                
                                                <div class="timeline-item-content">
                                                    <label class="col-md-3 control-label">Waktu: </label>
                                                        <div class="input-group">
                                                            <input type="text" id="mulaisd" name="mulaisd" class="form-control datepicker" value="2015-01-01"/>
                                                            <span class="input-group-addon add-on"> - </span>
                                                            <input type="text" id="selesaisd" name="selesaisd" class="form-control datepicker" value="2015-01-05"/>
                                                        </div>   
                                                    <label class="col-md-3 control-label">Nama: </label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                                            <input type="text" id="namasd" name="namasd" class="form-control" placeholder="Nama institusi"/>
                                                        </div>                
                                                </div>                                    
                                            </div>       
                                            <!-- SD --> 

                                            <!-- SMP -->
                                            <div class="timeline-item timeline-main">
                                                <div class="timeline-date">SMP</div>
                                            </div>
                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon"><span class="fa fa-info-circle"></span></div>                                   
                                                <div class="timeline-item-content">
                                                    <label class="col-md-3 control-label">Waktu: </label>
                                                        <div class="input-group">
                                                            <input type="text" id="mulaismp" name="mulaismp" class="form-control datepicker" value="2015-01-01"/>
                                                            <span class="input-group-addon add-on"> - </span>
                                                            <input type="text" id="selesaismp" name="selesaismp" class="form-control datepicker" value="2015-01-05"/>
                                                        </div>   
                                                    <label class="col-md-3 control-label">Nama: </label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                                            <input type="text" id="namasmp" name="namasmp" class="form-control" placeholder="Nama institusi"/>
                                                        </div>    
                                                </div>                                    
                                            </div>       
                                            <!-- SMP -->  

                                            <!-- SMA -->
                                            <div class="timeline-item timeline-main">
                                                <div class="timeline-date">SMA/SMK</div>
                                            </div>
                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon"><span class="fa fa-info-circle"></span></div>                                   
                                                <div class="timeline-item-content">
                                                    <label class="col-md-3 control-label">Waktu: </label>
                                                        <div class="input-group">
                                                            <input type="text" id="mulaisma" name="mulaisma" class="form-control datepicker" value="2015-01-01"/>
                                                            <span class="input-group-addon add-on"> - </span>
                                                            <input type="text" id="selesaisma" name="selesaisma" class="form-control datepicker" value="2015-01-05"/>
                                                        </div>   
                                                    <label class="col-md-3 control-label">Nama: </label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                                            <input type="text" id="namasma" name="namasma" class="form-control" placeholder="Nama institusi"/>
                                                        </div>    
                                                </div>                                    
                                            </div>       
                                            <!-- SMA -->

                                            <!-- S1 -->
                                            <div class="timeline-item timeline-main">
                                                <div class="timeline-date">S1</div>
                                            </div>
                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon"><span class="fa fa-info-circle"></span></div>                                   
                                                <div class="timeline-item-content">
                                                    <label class="col-md-3 control-label">Waktu: </label>
                                                        <div class="input-group">
                                                            <input type="text" id="mulais1" name="mulais1" class="form-control datepicker" value="2015-01-01"/>
                                                            <span class="input-group-addon add-on"> - </span>
                                                            <input type="text" id="selesais1" name="selesais1" class="form-control datepicker" value="2015-01-05"/>
                                                        </div>   
                                                    <label class="col-md-3 control-label">Nama: </label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                                            <input type="text" id="namas1" name="namas1" class="form-control" placeholder="Nama institusi"/>
                                                        </div>    
                                                </div>                                    
                                            </div>       
                                            <!-- S1 -->                    
                                        </div>
                                        <div class="col-md-7"></div>
                                        <!-- BUTTON SUBMIT EDUCATION FORM -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary btn-rounded pull-right">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END BUTTON SUBMIT EDUCATION FORM -->
                                    </div>
                                </form>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                        <!-- END EDUCATION TAB -->
                        <!-- PORTFOLIO TAB -->
                        <div class="tab-pane" id="tab9">
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="btn btn-default btn-condensed pull-right" id="tambahPortfolio"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button class="btn btn-default btn-condensed pull-right" id="hapusPortfolio"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <br><br><br><br>
                                <div class="col-md-12">
                                    <form enctype="multipart/form-data" class="form-horizontal">
                                        <!-- FORM PORTFOLIO -->
                                        <div id = "pagetambahPortfolio">
                                            <p>Portfolio 1</p> 
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>Cover</label><br/>
                                                    <input type="file" multiple id="file-simple1"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Name: </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                        <input type="text" class="form-control" placeholder="Nama Portfolio"/>
                                                    </div> 
                                                    <br>
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <div>
                                                            <textarea class="form-control" rows="3" placeholder="Your description type here.."></textarea>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <!--</div>
                                        <p>Portfolio 2</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Cover</label><br/>
                                                <input type="file" multiple id="file-simple1"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name: </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" class="form-control" placeholder="Nama institusi"/>
                                                </div> 
                                                <br>
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <div>
                                                        <textarea class="form-control" rows="3" placeholder="Your description type here.."></textarea>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>
                                        <p>Portfolio 3</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Cover</label><br/>
                                                <input type="file" multiple id="file-simple2"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name: </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" class="form-control" placeholder="Nama institusi"/>
                                                </div> 
                                                <br>
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <div>
                                                        <textarea class="form-control" rows="3" placeholder="Your description type here.."></textarea>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>
                                        <p>Portfolio 4</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Cover</label><br/>
                                                <input type="file" multiple id="file-simple3"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name: </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" class="form-control" placeholder="Nama institusi"/>
                                                </div> 
                                                <br>
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <div>
                                                        <textarea class="form-control" rows="3" placeholder="Your description type here.."></textarea>
                                                    </div>
                                                </div>  
                                            </div> 
                                        </div> -->
                                        <br>
                                        <div class="form-group">
                                            <div class="col-md-6 col-xs-5">
                                                <button type="submit" class="btn btn-primary btn-rounded pull-right">Save</button>
                                            </div>
                                        </div>
                                        <!-- FORM PORTFOLIO -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END PORTFOLIO TAB -->
                    </div>
                    <!-- END TAB CONTENT -->
                </div>   
            </div> 
            <!-- END EDIT EDICATION-PORTFOLIO PAGE --> 
            </div>            
            <!-- END PAGE CONTENT -->  
            @endsection

        @section('script')
        <script>
            $(function(){
                //Spinner
                $(".spinner_default").spinner()
                $(".spinner_decimal").spinner({step: 0.01, numberFormat: "n"});                
                //End spinner
                
                //Datepicker
                $('#dp-2').datepicker();
                $('#dp-3').datepicker({startView: 2});
                $('#dp-4').datepicker({startView: 1});                
                //End Datepicker
            });

            // File upload
            $("#file-simple1").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });            
            // End File upload

            // Tambah Portfolio
            $(document).ready(function() {
                var count = 1;

                $("#tambahPortfolio").click(function(){
                    count += 1; 
                    $('#pagetambahPortfolio').append(
                        '<p id="recordPortfolio'+ count +'">Portfolio '+ count +'</p> '
                        +'<div id="formPortfolio'+ count +'" class="form-group">'
                        +   '<div class="col-md-6">'
                        +       '<label>Cover</label><br/>'
                        +        '<input type="file" multiple id="file-simple'+ count +'"/>'
                        +    '</div>'
                        +    '<div class="col-md-6">'
                        +        '<label>Name: </label>'
                        +        '<div class="input-group">'
                        +            '<span class="input-group-addon"><i class="fa fa-cogs"></i></span>'
                        +            '<input type="text" class="form-control" placeholder="Name Portfolio"/>'
                        +        '</div>' 
                        +        '<br>'
                        +       '<div class="form-group">'
                        +            '<label>Description</label>'
                        +            '<div>'
                        +                '<textarea class="form-control" rows="3" placeholder="Your description type here.."></textarea>'
                        +            '</div>'
                        +        '</div>' 
                        +    '</div>' 
                        +'</div>'
                    ); 
                    $("#file-simple"+count).fileinput({
                        showUpload: false,
                        showCaption: false,
                        browseClass: "btn btn-danger",
                        fileType: "any"
                    });  
                });
             
                $("#hapusPortfolio").click(function(){
                    $('#recordPortfolio'+ count +'').remove();
                    $('#formPortfolio'+ count +'').remove();
                    $('#recordPortfolio'+ count +'').fadeOut('slow');
                    $('#formPortfolio'+ count +'').fadeOut('slow');
                    count -= 1;
                    if(count<2){
                        count=1;
                    }
                });
            });
        </script>
        @endsection







@extends('layout.app')
@section('title') Organizations - Experiences @endsection                 
             
            @section('content') 
            <!-- START PAGE CONTENT -->
            <div class="page-content">
            <!-- LOCK SCREEEN AND SIGN OUT BUTTON -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button">
                    <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li>
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
            </ul>
            <!-- END LOCK SCREEEN AND SIGN OUT BUTTON -->                  

            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Organizations-Experiences</a></li>
                <li class="active">Edit</li>
            </ul>
            <!-- END BREADCRUMB -->                 

            <!-- START EDIT EDICATION-PORTFOLIO PAGE -->
            <div class="page-title">                    
                <h2><span class="fa fa-users"></span> Organizations-Experiences</h2>
            </div>                   
            <div class="page-content-wrap">
                <div class="panel panel-default tabs">
                    <!-- NAV TAB -->
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab8" data-toggle="tab">Organizations</a></li>
                        <li><a href="#tab9" data-toggle="tab">Experiences</a></li>
                    </ul>
                    <!-- END NAV TAB -->
                    <!-- TAB CONTENT -->
                    <div class="panel-body tab-content">
                        <!-- ORGANIZATIOM TAB -->
                        <div class="tab-pane active" id="tab8">
                            <div class="row">  
                                <div class="col-md-6">
                                    <button class="btn btn-default btn-condensed pull-right" id="tambahOrganization"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button class="btn btn-default btn-condensed pull-right" id="hapusOrganization"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <br><br><br><br>
                                <form enctype="multipart/form-data" class="form-horizontal">
                                    <div class="col-md-11">
                                        <div class="timeline timeline-right">
                                            <div id="pagetambahOrganization">

                                                <div class="timeline-item timeline-item-right">
                                                    <div class="timeline-item-icon">1</div>                                
                                                    <div class="timeline-item-content">
                                                        <label class="col-md-3 control-label">Time: </label>
                                                        <div class="input-group">
                                                            <input type="text" id="mulaiorgan1" name="mulaiorgan1" class="form-control datepicker" value="2015-01-01"/>
                                                            <span class="input-group-addon add-on"> - </span>
                                                            <input type="text" id="selesaiorgan1" name="selesaiorgan1" class="form-control datepicker" value="2015-01-05"/>
                                                        </div>   
                                                        <label class="col-md-3 control-label">Name: </label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-suitcase"></i></span>
                                                            <input type="text" id="namaorgan1" name="namaorgan1" class="form-control" placeholder="Organization Name"/>
                                                        </div> 
                                                        <label class="col-md-3 control-label">Positions: </label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-certificate"></i></span>
                                                            <input type="text" id="posisiorgan1" name="posisiorgan1" class="form-control" placeholder="Your Positions"/>
                                                        </div>                
                                                    </div>                                    
                                                </div> 

                                            </div>

                                            <!--<div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon">2</div>             
                                                <div class="timeline-item-content">
                                                    <label class="col-md-3 control-label">Time: </label>
                                                    <div class="input-group">
                                                        <input type="text" id="mulaiorgan2" name="mulaiorgan2" class="form-control datepicker" value="2015-01-01"/>
                                                        <span class="input-group-addon add-on"> - </span>
                                                        <input type="text" id="selesaiorgan2" name="selesaiorgan2" class="form-control datepicker" value="2015-01-05"/>
                                                    </div>   
                                                    <label class="col-md-3 control-label">Name: </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-suitcase"></i></span>
                                                        <input type="text" id="namaorgan2" name="namaorgan2" class="form-control" placeholder="Organization Name"/>
                                                    </div> 
                                                    <label class="col-md-3 control-label">Positions: </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-certificate"></i></span>
                                                        <input type="text" id="posisiorgan2" name="posisiorgan2" class="form-control" placeholder="Your Positions"/>
                                                    </div>     
                                                </div>                                    
                                            </div> 

                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon">3</div>                 
                                                <div class="timeline-item-content">
                                                    <label class="col-md-3 control-label">Time: </label>
                                                    <div class="input-group">
                                                        <input type="text" id="mulaiorgan3" name="mulaiorgan3" class="form-control datepicker" value="2015-01-01"/>
                                                        <span class="input-group-addon add-on"> - </span>
                                                        <input type="text" id="selesaiorgan3" name="selesaiorgan3" class="form-control datepicker" value="2015-01-05"/>
                                                    </div>   
                                                    <label class="col-md-3 control-label">Name: </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-suitcase"></i></span>
                                                        <input type="text" id="namaorgan3" name="namaorgan3" class="form-control" placeholder="Organization Name"/>
                                                    </div> 
                                                    <label class="col-md-3 control-label">Positions: </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-certificate"></i></span>
                                                        <input type="text" id="posisiorgan3" name="posisiorgan3" class="form-control" placeholder="Your Positions"/>
                                                    </div>     
                                                </div>                                    
                                            </div>  

                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon">4</div> 
                                                <div class="timeline-item-content">
                                                    <label class="col-md-3 control-label">Time: </label>
                                                    <div class="input-group">
                                                        <input type="text" id="mulaiorgan4" name="mulaiorgan4" class="form-control datepicker" value="2015-01-01"/>
                                                        <span class="input-group-addon add-on"> - </span>
                                                        <input type="text" id="selesaiorgan4" name="selesaiorgan4" class="form-control datepicker" value="2015-01-05"/>
                                                    </div>   
                                                    <label class="col-md-3 control-label">Name: </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-suitcase"></i></span>
                                                        <input type="text" id="namaorgan4" name="namaorgan4" class="form-control" placeholder="Organization Name"/>
                                                    </div> 
                                                    <label class="col-md-3 control-label">Positions: </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-certificate"></i></span>
                                                        <input type="text" id="posisiorgan4" name="posisiorgan4" class="form-control" placeholder="Your Positions"/>
                                                    </div>  
                                                </div>                                    
                                            </div>-->  

                                        </div>
                                        <div class="col-md-7"></div>
                                        <!-- BUTTON SUBMIT EDUCATION FORM -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary btn-rounded pull-right">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END BUTTON SUBMIT EDUCATION FORM -->
                                    </div>
                                </form>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                        <!-- END ORGANIZATION TAB -->
                        <!-- EXPERIENCES TAB -->
                        <div class="tab-pane" id="tab9">
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="btn btn-default btn-condensed pull-right" id="tambahExperiences"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button class="btn btn-default btn-condensed pull-right" id="hapusExperiences"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <br><br><br><br>
                                <div class="col-md-12">
                                    <form enctype="multipart/form-data" class="form-horizontal">
                                        <!-- FORM EXPERIENCES -->
                                        <div id = "pagetambahExperiences">
                                            <p>Experience 1</p> 
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>Certificate</label><br/>
                                                    <input type="file" name="evidence1" multiple id="file-simple1"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Name </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                        <input type="text" id="experience1" name="experience1" class="form-control" placeholder="ex: Bina Cinta Lingkungan"/>
                                                    </div> 
                                                    <br>
                                                    <label>Year </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                        <input type="text" id="year_experience1" name="year_experience1" class="form-control" placeholder="ex: 2016"/>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <!--<p>Experience 2</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Certificate</label><br/>
                                                <input type="file" name="evidence2" multiple id="file-simple2"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="experience2" name="experience2" class="form-control" placeholder="ex: Bina Cinta Lingkungan"/>
                                                </div> 
                                                <br>
                                                <label>Year </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="year_experience2" name="year_experience2" class="form-control" placeholder="ex: 2016"/>
                                                </div> 
                                            </div> 
                                        </div>
                                        <p>Experience 3</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Certificate</label><br/>
                                                <input type="file" name="evidence3" multiple id="file-simple3"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="experience3" name="experience3" class="form-control" placeholder="ex: Bina Cinta Lingkungan"/>
                                                </div> 
                                                <br>
                                                <label>Year </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="year_experience3" name="year_experience3" class="form-control" placeholder="ex: 2016"/>
                                                </div> 
                                            </div> 
                                        </div>
                                        <p>Experience 4</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Certificate</label><br/>
                                                <input type="file" name="evidence4" multiple id="file-simple4"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="experience4" name="experience4" class="form-control" placeholder="ex: Bina Cinta Lingkungan"/>
                                                </div> 
                                                <br>
                                                <label>Year </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="year_experience4" name="year_experience4" class="form-control" placeholder="ex: 2016"/>
                                                </div> 
                                            </div> 
                                        </div>-->

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-rounded pull-right">Save</button>
                                            </div>
                                        </div>
                                        <!-- FORM EXPERIENCES -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END EXPERIENCES TAB -->
                    </div>
                    <!-- END TAB CONTENT -->
                </div>   
            </div> 
            <!-- END EDIT EDICATION-PORTFOLIO PAGE --> 
            </div>            
            <!-- END PAGE CONTENT -->
            @endsection

        @section('script')
        <script>
            $(function(){
                //Spinner
                $(".spinner_default").spinner()
                $(".spinner_decimal").spinner({step: 0.01, numberFormat: "n"});                
                //End spinner
                
                //Datepicker
                $('#dp-2').datepicker();
                $('#dp-3').datepicker({startView: 2});
                $('#dp-4').datepicker({startView: 1});                
                //End Datepicker
            });

            // File upload
            $("#file-simple1").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });  
            $("#file-simple2").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });  
            $("#file-simple3").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });  
            $("#file-simple4").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });             
            // End File upload

            // Tambah Organization
            $(document).ready(function() {
                var count = 1;

                $("#tambahOrganization").click(function(){
                    count += 1; 
                    $('#pagetambahOrganization').append(
                        '<div id="recordOrganization'+ count +'" class="timeline-item timeline-item-right">'
                        +    '<div class="timeline-item-icon">'+count+'</div>'                                
                        +    '<div class="timeline-item-content">'
                        +        '<label class="col-md-3 control-label">Time: </label>'
                        +        '<div class="input-group">'
                        +            '<input type="text" id="mulaiorgan1" name="mulaiorgan1" class="form-control datepicker" value="2015-01-01"/>'
                        +            '<span class="input-group-addon add-on"> - </span>'
                        +            '<input type="text" id="selesaiorgan1" name="selesaiorgan1" class="form-control datepicker" value="2015-01-05"/>'
                        +        '</div>'   
                        +        '<label class="col-md-3 control-label">Name: </label>'
                        +        '<div class="input-group">'
                        +            '<span class="input-group-addon"><i class="fa fa-suitcase"></i></span>'
                        +            '<input type="text" id="namaorgan1" name="namaorgan1" class="form-control" placeholder="Organization Name"/>'
                        +        '</div>'
                        +        '<label class="col-md-3 control-label">Positions: </label>'
                        +        '<div class="input-group">'
                        +            '<span class="input-group-addon"><i class="fa fa-certificate"></i></span>'
                        +            '<input type="text" id="posisiorgan1" name="posisiorgan1" class="form-control" placeholder="Your Positions"/>'
                        +        '</div>'                
                        +    '</div>'                                    
                        +'</div>' 
                    );
                });
             
                $("#hapusOrganization").click(function(){
                    $('#recordOrganization'+ count +'').remove();
                    $('#recordOrganization'+ count +'').fadeOut('slow');
                    count -= 1;
                    if(count<2){
                        count=1;
                    }
                });
            });

            // Tambah Experiences
            $(document).ready(function() {
                var count = 1;

                $("#tambahExperiences").click(function(){
                    count += 1; 
                    $('#pagetambahExperiences').append(
                        '<p id="recordExperiences'+count+'">Experience '+count+'</p>' 
                    +    '<div id="formExperiences'+count+'" class="form-group">'
                    +        '<div class="col-md-6">'
                    +            '<label>Certificate</label><br/>'
                    +            '<input type="file" name="evidence1" multiple id="file-simple'+count+'"/>'
                    +        '</div>'
                    +        '<div class="col-md-6">'
                    +            '<label>Name </label>'
                    +            '<div class="input-group">'
                    +                '<span class="input-group-addon"><i class="fa fa-cogs"></i></span>'
                    +                '<input type="text" id="experience1" name="experience1" class="form-control" placeholder="ex: Bina Cinta Lingkungan"/>'
                    +            '</div>'
                    +            '<br>'
                    +            '<label>Year </label>'
                    +            '<div class="input-group">'
                    +                '<span class="input-group-addon"><i class="fa fa-cogs"></i></span>'
                    +                '<input type="text" id="year_experience1" name="year_experience1" class="form-control" placeholder="ex: 2016"/>'
                    +            '</div>' 
                    +        '</div>' 
                    +    '</div>'
                    );
                    $("#file-simple"+count).fileinput({
                        showUpload: false,
                        showCaption: false,
                        browseClass: "btn btn-danger",
                        fileType: "any"
                    });  
                });
             
                $("#hapusExperiences").click(function(){
                    $('#recordExperiences'+ count +'').remove();
                    $('#formExperiences'+ count +'').remove();
                    $('#recordExperiences'+ count +'').fadeOut('slow');
                    $('#formExperiences'+ count +'').fadeOut('slow');
                    count -= 1;
                    if(count<2){
                        count=1;
                    }
                });
            });
        </script>   
        @endsection






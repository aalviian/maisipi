@extends('layout.app')
@section('title') Skills - Achievements @endsection                 
             
            @section('content') 
            <!-- START PAGE CONTENT -->
            <div class="page-content">
            <!-- LOCK SCREEEN AND SIGN OUT BUTTON -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button">
                    <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <li class="xn-icon-button pull-right last">
                    <a href="#"><span class="fa fa-power-off"></span></a>
                    <ul class="xn-drop-left animated zoomIn">
                        <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li>
                        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                    </ul>                        
                </li> 
            </ul>
            <!-- END LOCK SCREEEN AND SIGN OUT BUTTON -->                  

            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Skills-Achievements</a></li>
                <li class="active">Edit</li>
            </ul>
            <!-- END BREADCRUMB -->                 

            <!-- START EDIT EDICATION-PORTFOLIO PAGE -->
            <div class="page-title">                    
                <h2><span class="fa fa-magic"></span> Skills-Achievements</h2>
            </div>                   
            <div class="page-content-wrap">
                <div class="panel panel-default tabs">
                    <!-- NAV TAB -->
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab8" data-toggle="tab">Skills</a></li>
                        <li><a href="#tab9" data-toggle="tab">Achievements</a></li>
                    </ul>
                    <!-- END NAV TAB -->
                    <!-- TAB CONTENT -->
                    <div class="panel-body tab-content">
                        <!-- SKILL TAB -->
                        <div class="tab-pane active" id="tab8">
                            <div class="row">  
                                <div class="col-md-6">
                                    <button class="btn btn-default btn-condensed pull-right" id="tambahSkill"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button class="btn btn-default btn-condensed pull-right" id="hapusSkill"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <br><br><br><br>
                                <form enctype="multipart/form-data" class="form-horizontal">
                                    <div class="col-md-9 col-sm-1">
                                        <div class="timeline timeline-right">

                                            <div id="pagetambahSkill">
                                                <div class="timeline-item timeline-item-right">
                                                    <div class="timeline-item-icon">1</div>                                
                                                    <div class="timeline-item-content">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Skill</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="skill1" name ="skill1" class="form-control" placeholder="ex: Programming"/>
                                                            </div>
                                                            <label class="col-md-3 control-label">Percentage</label>
                                                            <div class="col-md-9">
                                                                 <input type="text" id="ise_min_max1" name="percentage1" value="" />  
                                                            </div>
                                                        </div>            
                                                    </div>                                    
                                                </div>
                                            </div> 

                                            <!--<div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon">2</div>             
                                                <div class="timeline-item-content">
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-5 control-label">Skill</label>
                                                        <div class="col-md-9 col-xs-7">
                                                            <input type="text" id="skill2" name ="skill2" class="form-control" placeholder="ex: Programming"/>
                                                        </div>
                                                        <label class="col-md-3 col-xs-5 control-label">Percentage</label>
                                                        <div class="col-md-9 col-xs-7">
                                                             <input type="text" id="ise_min_max2" name="percentage2" value="" />  
                                                        </div>
                                                    </div>    
                                                </div>                                    
                                            </div> 

                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon">3</div>                 
                                                <div class="timeline-item-content">
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-5 control-label">Skill</label>
                                                        <div class="col-md-9 col-xs-7">
                                                            <input type="text" id="skill3" name ="skill3" class="form-control" placeholder="ex: Programming"/>
                                                        </div>
                                                        <label class="col-md-3 col-xs-5 control-label">Percentage</label>
                                                        <div class="col-md-9 col-xs-7">
                                                             <input type="text" id="ise_min_max3" name="percentage3" value="" />  
                                                        </div>
                                                    </div>    
                                                </div>                                    
                                            </div>  

                                            <div class="timeline-item timeline-item-right">
                                                <div class="timeline-item-icon">4</div> 
                                                <div class="timeline-item-content">
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-5 control-label">Skill</label>
                                                        <div class="col-md-9 col-xs-7">
                                                            <input type="text" id="skill4" name ="skill4" class="form-control" placeholder="ex: Programming"/>
                                                        </div>
                                                        <label class="col-md-3 col-xs-5 control-label">Percentage</label>
                                                        <div class="col-md-9 col-xs-7">
                                                             <input type="text" id="ise_min_max4" name="percentage4" value="" />  
                                                        </div>
                                                    </div>
                                                </div>                                    
                                            </div>-->    

                                        </div>
                                        <div class="col-md-7"></div>
                                        <!-- BUTTON SUBMIT EDUCATION FORM -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary btn-rounded pull-right">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END BUTTON SUBMIT EDUCATION FORM -->
                                    </div>
                                </form>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                        <!-- END SKILL TAB -->
                        <!-- ACHIEVEMENT TAB -->
                        <div class="tab-pane" id="tab9">
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="btn btn-default btn-condensed pull-right" id="tambahAchievement"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button class="btn btn-default btn-condensed pull-right" id="hapusAchievement"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <br><br><br><br>
                                <div class="col-md-12">
                                    <form enctype="multipart/form-data" class="form-horizontal">
                                        <!-- FORM ACHIEVEMENTS -->
                                        <div id="pagetambahAchievement">
                                            <p>Achievement 1</p> 
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>Certificate</label><br/>
                                                    <input type="file" name="certificate1" multiple id="file-simple1"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Name </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                        <input type="text" id="achievement1" name="achievement1" class="form-control" placeholder="ex: Kompetisi Nasional Bidang Informatika"/>
                                                    </div> 
                                                    <br>
                                                    <label>Year </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                        <input type="text" id="year_achievement1" name="year_achievement1" class="form-control" placeholder="ex: 2016"/>
                                                    </div>
                                                    <br>
                                                    <label>Description </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                        <input type="text" id="description1" name="description1" class="form-control" placeholder="Tell your description"/>
                                                    </div>  
                                                </div> 
                                            </div>
                                        </div>
                                        <!--<p>Achievement 2</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Certificate</label><br/>
                                                <input type="file" name="certificate2" multiple id="file-simple2"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="achievement2" name="achievement2" class="form-control" placeholder="ex: Kompetisi Nasional Bidang Informatika"/>
                                                </div> 
                                                <br>
                                                <label>Year </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="year_achievement2" name="year_achievement2" class="form-control" placeholder="ex: 2016"/>
                                                </div> 
                                            </div> 
                                        </div>
                                        <p>Achievement 3</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Certificate</label><br/>
                                                <input type="file" name="certificate3" multiple id="file-simple3"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="achievement3" name="achievement3" class="form-control" placeholder="ex: Kompetisi Nasional Bidang Informatika"/>
                                                </div> 
                                                <br>
                                                <label>Year </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="year_achievement3" name="year_achievement3" class="form-control" placeholder="ex: 2016"/>
                                                </div> 
                                            </div> 
                                        </div>
                                        <p>Achievement 4</p> 
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>Certificate</label><br/>
                                                <input type="file" name="certificate4" multiple id="file-simple4"/>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Name </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="achievement4" name="achievement4" class="form-control" placeholder="ex: Kompetisi Nasional Bidang Informatika"/>
                                                </div> 
                                                <br>
                                                <label>Year </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                                    <input type="text" id="year_achievement4" name="year_achievement4" class="form-control" placeholder="ex: 2016"/>
                                                </div> 
                                            </div> 
                                        </div>-->

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-rounded pull-right">Save</button>
                                            </div>
                                        </div>
                                        <!-- FORM ACHIEVEMENTS -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END ACHIEVEMENT TAB -->
                    </div>
                    <!-- END TAB CONTENT -->
                </div>   
            </div> 
            <!-- END EDIT EDICATION-PORTFOLIO PAGE --> 
            </div>            
            <!-- END PAGE CONTENT -->
            @endsection

        @section('script')
        <script>
            $(function(){
                //Spinner
                $(".spinner_default").spinner()
                $(".spinner_decimal").spinner({step: 0.01, numberFormat: "n"});                
                //End spinner
                
                //Datepicker
                $('#dp-2').datepicker();
                $('#dp-3').datepicker({startView: 2});
                $('#dp-4').datepicker({startView: 1});                
                //End Datepicker
            });

            // File upload
            $("#file-simple1").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });  
            $("#file-simple2").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });  
            $("#file-simple3").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });  
            $("#file-simple4").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });             
            // End File upload

            //Min Max Value
            $("#ise_min_max1").ionRangeSlider({
                min: 0,
                max: 100,
                from: 0
            });


            // Tambah Skill
            $(document).ready(function() {
                var count = 1;

                $("#tambahSkill").click(function(){
                    count += 1; 
                    $('#pagetambahSkill').append(
                        '<div id="recordSkill'+count+'" class="timeline-item timeline-item-right">'
                    +        '<div class="timeline-item-icon">'+count+'</div>'                                
                    +        '<div class="timeline-item-content">'
                    +            '<div class="form-group">'
                    +                '<label class="col-md-3 control-label">Skill</label>'
                    +                '<div class="col-md-9">'
                    +                    '<input type="text" id="skill1" name ="skill1" class="form-control" placeholder="ex: Programming"/>'
                    +                '</div>'
                    +                '<label class="col-md-3 control-label">Percentage</label>'
                    +                '<div class="col-md-">'
                    +                     '<input type="text" id="ise_min_max'+count+'" name="percentage1" value="" />' 
                    +                '</div>'
                    +            '</div>'            
                    +        '</div>'                                    
                    +    '</div>'
                    );
                    $("#ise_min_max"+count).ionRangeSlider({
                        min: 0,
                        max: 100,
                        from: 0
                    });
                });
             
                $("#hapusSkill").click(function(){
                    $('#recordSkill'+ count +'').remove();
                    $('#recordSkill'+ count +'').fadeOut('slow');
                    count -= 1;
                    if(count<2){
                        count=1;
                    }
                });
            });

            // Tambah Achieviement
            $(document).ready(function() {
                var count = 1;

                $("#tambahAchievement").click(function(){
                    count += 1; 
                    $('#pagetambahAchievement').append(
                        '<p id="recordAchievement'+count+'">Achievement '+count+'</p>' 
                    +    '<div id="formAchievement'+count+'" class="form-group">'
                    +        '<div class="col-md-6">'
                    +            '<label>Certificate</label><br/>'
                    +            '<input type="file" name="certificate1" multiple id="file-simple'+count+'"/>'
                    +        '</div>'
                    +        '<div class="col-md-6">'
                    +            '<label>Name </label>'
                    +            '<div class="input-group">'
                    +                '<span class="input-group-addon"><i class="fa fa-cogs"></i></span>'
                    +                '<input type="text" id="achievement1" name="achievement1" class="form-control" placeholder="ex: Kompetisi Nasional Bidang Informatika"/>'
                    +            '</div>' 
                    +            '<br>'
                    +            '<label>Year </label>'
                    +            '<div class="input-group">'
                    +                '<span class="input-group-addon"><i class="fa fa-cogs"></i></span>'
                    +                '<input type="text" id="year_achievement1" name="year_achievement1" class="form-control" placeholder="ex: 2016"/>'
                    +            '</div>'
                    +            '<br>'
                    +            '<label>Description </label>'
                    +            '<div class="input-group">'
                    +                '<span class="input-group-addon"><i class="fa fa-cogs"></i></span>'
                    +                '<input type="text" id="description1" name="description1" class="form-control" placeholder="Tell your description"/>'
                    +            '</div>'   
                    +        '</div>' 
                    +    '</div>'
                    );
                    $("#file-simple"+count).fileinput({
                        showUpload: false,
                        showCaption: false,
                        browseClass: "btn btn-danger",
                        fileType: "any"
                    });  
                });
             
                $("#hapusAchievement").click(function(){
                    $('#recordAchievement'+ count +'').remove();
                    $('#formAchievement'+ count +'').remove();
                    $('#recordAchievement'+ count +'').fadeOut('slow');
                    $('#formAchievement'+ count +'').fadeOut('slow');
                    count -= 1;
                    if(count<2){
                        count=1;
                    }
                });
            });
        </script>
        @endsection







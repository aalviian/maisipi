@extends('layout.app')
@section('title') Profile @endsection                 
             
        @section('content')    
            <!-- START PAGE CONTENT -->     
            <div class="page-content">
                <!-- LOCK SCREEEN AND SIGN OUT BUTTON -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                            <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li>
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                </ul>
                <!-- END LOCK SCREEEN AND SIGN OUT BUTTON -->                  
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Profile</a></li>
                    <li class="active">Edit</li>
                </ul>
                <!-- END BREADCRUMB -->                

                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default tabs">
                                <!-- NAV TAB -->
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab">General</a></li>
                                    <li><a href="#tab2" data-toggle="tab">Privacy</a></li>
                                </ul>
                                <!-- END NAV TAB -->
                                <!-- TAB CONTENT -->
                                <div class="panel-body tab-content">
                                    <!-- GENERAL TAB -->
                                    <div class="tab-pane active" id="tab1">
                                        <div class="row">
                                            <form action="#" method ="post" class="form-horizontal">
                                                <!-- BOX FULLNAME, SURNAME, LOCATION, ABOUT ME-->
                                                <div class="col-md-6">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <h3><span class="fa fa-pencil"></span> General</h3>
                                                        </div>
                                                        <div class="panel-body form-group-separated">
                                                            <!-- FORM FULL NAME -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">Full Name</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <input type="text" id="fullname" name ="fullname" class="form-control" placeholder="Type your fullname.."/>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM FULL NAME -->
                                                            <!-- FORM SURNAME -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">Surname</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <input type="text" id="surname" name="surname" class="form-control" placeholder="Type your surname.."/>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM SURNAME -->
                                                            <!-- FORM LOCATION -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">Location</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <input type="text" id="location" name="location" class="form-control" placeholder="Type your country.. (ex: Tangerang)" />
                                                                </div>
                                                            </div>
                                                            <!-- END FORM LOCATION -->
                                                            <!-- FORM ABOUT ME -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">About me</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <textarea class="form-control" id="aboutme" name="aboutme" rows="5" placeholder="About yourself type here.."></textarea>
                                                                </div>
                                                            </div>  
                                                            <!-- END FORM ABOUT ME -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END BOX FULLNAME, SURNAME, LOCATION, ABOUT ME-->
                                                <!-- BOX PLACE, BIRTHDATE, ADDRESS, RELIGION-->
                                                <div class="col-md-6">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                        </div>
                                                        <div class="panel-body">
                                                        </div>
                                                        <div class="panel-body form-group-separated">
                                                            <!-- FORM PLACE OF BIRTH -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">Place of Birth</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <input type="text" id="placebirth" nname="placebirth" class="form-control" placeholder="Type your place of birth.."/>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM PLACE OF BIRTH -->
                                                            <!-- FORM BIRTHDATE -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">Birth Date</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <input type="text" id="birthdate" name="birthdate" class="form-control datepicker" value="2015-01-01"/>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM BIRTHDATE -->
                                                            <!-- FORM ADDRESS -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">Address</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <input type="text" id="address" name="address" class="form-control" placeholder="Type your address.."/>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM ADDRESS -->
                                                            <!-- FORM RELIGION -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 col-xs-5 control-label">Religion</label>
                                                                <div class="col-md-9 col-xs-7">
                                                                    <input type="text" id="religion" name="religion" class="form-control" placeholder="Type your religion.."/>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM RELIGION -->
                                                            <!-- BUTTON SUBMIT FOR GENERAL TAB -->  
                                                            <div class="form-group">
                                                                <div class="col-md-12 col-xs-5">
                                                                    <button class="btn btn-primary btn-rounded pull-right">Save</button>
                                                                </div>
                                                            </div>
                                                            <!-- BUTTON SUBMIT FOR GENERAL TAB -->  
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END BOX PLACE, BIRTHDATE, ADDRESS, RELIGION-->
                                            </form>
                                        </div>
                                    </div>
                                    <!-- END GENERAL TAB -->
                                    <!-- PRIVACY TAB -->
                                    <div class="tab-pane" id="tab2">
                                        <div class="row">
                                            <!-- BOX FOR NAME AND IMAGE USER -->
                                            <div class="col-md-6">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <h3><span class="fa fa-user"></span> Indra Sugiarto</h3>
                                                        <p>Owner Katalis & Ruang Ananta</p>
                                                        <div class="text-center" id="user_image">
                                                            <img src="{{ asset('assets/images/users/avatar.jpg') }}" class="img-thumbnail"/>
                                                        </div>                                    
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END BOX FOR NAME AND IMAGE USER -->

                                            <!-- BOX FOR EDIT PRIVACY -->
                                            <div class="col-md-6">
                                                <!-- FORM PASSWORD -->
                                                <div class="panel-body form-group-separated">
                                                    <div class="form-group">                               
                                                        <div class="col-md-12 col-xs-12">
                                                            <a href="#" class="btn btn-danger btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_password">Change password</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END FORM PASSWORD -->

                                                <!-- FORM USERNAME EMAIL AND PHOTO -->
                                                <form action="#" class="form-horizontal">
                                                    <div class="panel-body form-group-separated">
                                                        <!-- FORM USERNAME -->
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Username</label>
                                                            <div class="col-md-9 col-xs-7">
                                                                <input type="text" class="form-control" placeholder="Type your username.."/>
                                                            </div>
                                                        </div>
                                                        <!-- END FORM USERNAME -->
                                                        <!-- FORM EMAIL -->
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">E-mail</label>
                                                            <div class="col-md-9 col-xs-7">
                                                                <input type="text" class="form-control" placeholder="Type your email."/>
                                                            </div>
                                                        </div>
                                                        <!-- END FORM EMAIL -->
                                                        <!-- FORM PROFILE PHOTO-->
                                                        <div class="form-group">
                                                            <div class="col-md-12 >
                                                                <label class="col-md-3 col-xs-5 control-label">Profile Photo</label>
                                                                <div class="col-md-9">
                                                                    <input type="file" multiple id="file-simple"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END FORM PROFILE PHOTO-->
                                                        <!-- BUTTON SUBMIT FOR FORM USERNAME EMAIL AND PHOTO -->
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <button class="btn btn-primary btn-rounded pull-right">Save</button>
                                                            </div>
                                                        </div>
                                                        <!-- END BUTTON SUBMIT FOR FORM USERNAME EMAIL AND PHOTO -->
                                                    </div>
                                                </form>
                                                <!-- END FORM USERNAME EMAIL AND PHOTO -->
                                            </div>
                                            <!-- BOX FOR EDIT PRIVACY -->
                                        </div>
                                    </div>
                                    <!-- END PRIVACY TAB -->
                                </div>
                                <!-- END TAB CONTENT -->
                            </div>
                        </div>  
                    </div>                   
                </div> 
                <!-- END EDIT PROFILE  PAGE --> 
            </div>            
            <!-- END PAGE CONTENT -->
            @endsection

        @section('script')
        <script>
            // File Upload
            $("#file-simple").fileinput({
                    showUpload: false,
                    showCaption: false,
                    browseClass: "btn btn-danger",
                    fileType: "any"
            });  
            //END File Upload
        </script>  
        @endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


//ALL ABOUT VIEW
Route::get('/', function () {
    return view('index');
});
Route::get('/profile', 'UserController@index');
Route::get('/educations-portfolios', 'EduPortController@index');
Route::get('/organizations-experiences', 'OrganExpController@index');
Route::get('/skills-achievements', 'SkillAchiController@index');

// //ALL ABOUT API
// Route::group(['prefix' => 'api'], function() { 
// 	Route::resource('/users', 'Api\UserController');
// 	Route::resource('/educations', 'Api\EduController');
// 	Route::resource('/portfolios', 'Api\PortController');
// 	Route::resource('/organizations', 'Api\OrganController');
// 	Route::resource('/experiences', 'Api\ExpController');
// 	Route::resource('/skills', 'Api\SkillController');
// 	Route::resource('/achievements', 'Api\AchiController');
// });

// //ROUTE API COMBINATION Using ELOQUENT ONE TO MANY.
// //Ex: User hasMany Education, but Education just BelongsTo One User
// Route::group(['prefix' => 'api-comb'], function() {
// 	Route::get('user-edu/{user}', function($user){
// 		return $user->educations; //Retrieve Educations's user with id_user
// 	});
// 	Route::get('user-port/{user}', function($user){
// 		return $user->portfolios; //Retrieve Portfolios's user with id_user
// 	});
// 	Route::get('user-organ/{user}', function($user){
// 		return $user->organizations;
// 	});
// 	Route::get('user-exp/{user}', function($user){
// 		return $user->experiences;
// 	});
// 	Route::get('user-skill/{user}', function($user){
// 		return $user->skills;
// 	});
// 	Route::get('user-achi/{user}', function($user){
// 		return $user->achievements;
// 	});
// });




<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('/users', 'Api\UserController');
Route::resource('/educations', 'Api\EduController');
Route::resource('/portfolios', 'Api\PortController');
Route::resource('/organizations', 'Api\OrganController');
Route::resource('/experiences', 'Api\ExpController');
Route::resource('/skills', 'Api\SkillController');
Route::resource('/achievements', 'Api\AchiController');

Route::get('user-edu/{user}', function($user){
	return $user->educations; //Retrieve Educations's user with id_user
});
Route::get('user-port/{user}', function($user){
	return $user->portfolios; //Retrieve Portfolios's user with id_user
});
Route::get('user-organ/{user}', function($user){
	return $user->organizations;
});
Route::get('user-exp/{user}', function($user){
	return $user->experiences;
});
Route::get('user-skill/{user}', function($user){
	return $user->skills;
});
Route::get('user-achi/{user}', function($user){
	return $user->achievements;
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

This is built by Laravel 5.3

First Desain Maisipi

Demo :
http://guesswho.agri.web.id/cvp/public/

Step:

1. composer install/update (setalah itu muncul .env)

2. php artisan key:generate

3. Setting database di .env

4. php artisan migrate:refresh --seed

6. php artisan serve

7. Login dengan email : alvian@gmail.com, pass : admin

Screenshot :

1.
![1.PNG](https://bitbucket.org/repo/xKbngp/images/2136172263-1.PNG)

2.
![2.PNG](https://bitbucket.org/repo/xKbngp/images/4019211402-2.PNG)

3.
![3.PNG](https://bitbucket.org/repo/xKbngp/images/223940969-3.PNG)

4.
![4.PNG](https://bitbucket.org/repo/xKbngp/images/3892314054-4.PNG)

5.
![5.PNG](https://bitbucket.org/repo/xKbngp/images/2525128495-5.PNG)

6.
![6.PNG](https://bitbucket.org/repo/xKbngp/images/2320151211-6.PNG)

7.
![7.PNG](https://bitbucket.org/repo/xKbngp/images/1774816954-7.PNG)

8.
![9.PNG](https://bitbucket.org/repo/xKbngp/images/2057344273-9.PNG)

9.
![9.PNG](https://bitbucket.org/repo/xKbngp/images/525539796-9.PNG)

10.
![10.PNG](https://bitbucket.org/repo/xKbngp/images/1624030842-10.PNG)

11.
![11.PNG](https://bitbucket.org/repo/xKbngp/images/3034370197-11.PNG)

12.
![12.PNG](https://bitbucket.org/repo/xKbngp/images/500052831-12.PNG)
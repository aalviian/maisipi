<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class achievements extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('id_ID');
		$counter = 0;
		$cover = ["","osn_cerificate.png", "pkm_certificate.png", "gemastik_certificate.png", "robotcontest_certificate.png"];
		$name = ["","Olimpiade Sains Nasiona", "Pekan Kreatifitas Mahasiswa", "Gemastik Nasional", "Robotic Contest"];
		$description = ["","1st Ranking", "2nd Ranking", "3rd Ranking", "Best Performance"];

        $field = 1;
        for($i=1; $i<=10; $i++){
            if($field==5) {
                $field = 1;
            }
        	$achi = new \App\Achievement;
        	$achi->user_id = $i;
        	$achi->name = $name[$field];
            $achi->year = $faker->date($format = 'Y');
        	$achi->description = $description[$field];
        	$achi->save();
        	$counter++;
            $field++;
        } 

        $field = 4;
        for($i=1; $i<=10; $i++){
            if($field==0) {
                $field = 4;
            }
            $achi = new \App\Achievement;
            $achi->user_id = $i;
            $achi->name = $name[$field];
            $achi->year = $faker->date($format = 'Y');
            $achi->description = $description[$field];
            $achi->save();
            $counter++;
            $field--;
        } 
        $this->command->info("Successfully created ".$counter." achievements");
    }
}

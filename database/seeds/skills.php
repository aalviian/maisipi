<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class skills extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('id_ID');
		$counter = 0;
		$name = ["","Programming", "Communcation", "Data Analysis", "Presentation", "Microsoft Office", "Web Development"];

		$field = 1;
        for($i=1; $i<=10; $i++){
        	if($field==7) {
        		$field = 1;
        	}
	    	$skill = new \App\Skill;
	    	$skill->user_id = $i;
	    	$skill->name = $name[$field];
	    	$skill->percentage = $faker -> numberBetween($min = 10, $max = 100); 
	    	$skill->save();
	    	$counter++;
	    	$field++;
        } 

        $field = 6;
        for($i=1; $i<=10; $i++){
        	if($field==0) {
        		$field = 6;
        	}
        	$skill = new \App\Skill;
        	$skill->user_id = $i;
        	$skill->name = $name[$field];
        	$skill->percentage = $faker -> numberBetween($min = 10, $max = 100); 
        	$skill->save();
        	$counter++;
        	$field--;
        } 
        $this->command->info("Successfully created ".$counter." skills");
    }
}

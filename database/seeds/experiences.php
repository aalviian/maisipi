<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class experiences extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('id_ID');
		$counter = 0;
		$certificate = ["","Bina-Cinta-Lingkungan.png", "International-Debates.png", "Student-Exchange.png", "Mahasiswa-Berprestasi-Indonesia.png"];
		$name = ["","Bina Cinta Lingkungan", "International Debates", "Japan Student Exchange", "Mahasiswa Berprestasi Indonesia"];
		
		$field = 1;
        for($i=1; $i<=10; $i++){
        	if($field==5) {
        		$field = 1;
        	}
        	$exp = new \App\Experience;
        	$exp->user_id = $i;
        	$exp->name = $name[$field];
        	$exp->year = $faker->date($format = 'Y');
        	$exp->certificate = $certificate[$field]; 
        	$exp->save();
        	$counter++;
        	$field++;
        } 

		$field = 4;
        for($i=1; $i<=10; $i++){
        	if($field==0) {
        		$field = 4;
        	}
        	$exp = new \App\Experience;
        	$exp->user_id = $i;
        	$exp->name = $name[$field];
        	$exp->year = $faker->date($format = 'Y');
        	$exp->certificate = $certificate[$field]; 
        	$exp->save();
        	$counter++;
        } 
        $this->command->info("Successfully created ".$counter." experiences");
    }
}

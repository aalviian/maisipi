<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(users::class);
        $this->call(educations::class);
        $this->call(portfolios::class);
        $this->call(organizations::class);
        $this->call(experiences::class);
        $this->call(skills::class);
        $this->call(achievements::class);
    }
}

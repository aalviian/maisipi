<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class educations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    { 
    	$faker = Faker::create('id_ID');
		$counter = 0;
		$school = ["SD Santo Thomas", "SD BPK Penabur", "SD Santa Laurensia", "SD Thoma Aquino"];
        for($i=1; $i<=10; $i++){
        	$edu = new \App\Education;
        	$edu->user_id = $i;
        	$edu->name = $school[rand(0,3)];
        	$edu->start_date = $faker->date($format = 'Y-m-d');
        	$edu->finish_date = $faker->date($format = 'Y-m-d');
        	$edu->save();
        	$counter++;
        } 
        $this->command->info("Successfully created ".$counter." education 'Elementary School'");

        $counter = 0;
        $school2 = ["SMP Santo Thomas", "SMP BPK Penabur", "SMP Santa Laurensia", "SMP Thoma Aquino"];
        for($i=1; $i<=10; $i++){
            $edu = new \App\Education;
            $edu->user_id = $i;
            $edu->name = $school2[rand(0,3)];
            $edu->start_date = $faker->date($format = 'Y-m-d');
            $edu->finish_date = $faker->date($format = 'Y-m-d');
            $edu->save();
            $counter++;
        }
        $this->command->info("Successfully created ".$counter." education 'Junir Hih School'");
    }
}

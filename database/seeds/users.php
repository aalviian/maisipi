<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $counter = 0;
        $religion = ["moslem", "protestant", "christian", "buddhism", "hinduism", "gnosticism", "judaism", "jewish"];
        
        for($i=1; $i<=10; $i++){
        	$user = new \App\User; 
        	$user->fullname = $faker->name;
        	$user->surname = $faker->lastname;
        	$user->email = $faker->unique()->email;
        	$user->username = $faker->unique()->username;
        	$user->password = 'member';
        	$user->location = $faker->city;
        	$user->about_me = "Hello, I am a human";
        	$user->birth_date = $faker->date($format = 'Y-m-d', $max = '2003-12-12');
        	$user->address = $faker->address;
        	$user->religion = $religion[rand(0,7)];
        	$user->save();
        	$counter++;
        }
        $this->command->info("Successfully created ".$counter." users");
    }
}
<?php

use Illuminate\Database\Seeder; 
use Faker\Factory as Faker;

class organizations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('id_ID');
		$counter = 0;
		$name = ["","HIMALKOM", "HIMABIO", "GSB", "HIMAGRETO", "GUMATIKA"];
		$position = ["","Ketua Divisi HRD", "Staff Divisi Eksternal", "Ketua Divisi Edukasi", "Staff Divisi Humas", "Staff Divisi Edukasi"];

		$field = 1;
        for($i=1; $i<=10; $i++){
        	if($field==6) {
        		$field = 1;
        	}
        	$organ = new \App\Organization;
        	$organ->user_id = $i;
        	$organ->name = $name[$field];
        	$organ->start_date = $faker->date($format = 'Y-m-d');
        	$organ->finish_date = $faker->date($format = 'Y-m-d');
        	$organ->position = $position[$field];
        	$organ->save();
        	$counter++;
        	$field++;
        } 

        $field = 5;
        for($i=1; $i<=10; $i++){
        	if($field==0) {
        		$field = 5;
        	}
        	$organ = new \App\Organization;
        	$organ->user_id = $i;
        	$organ->name = $name[$field];
        	$organ->start_date = $faker->date($format = 'Y-m-d');
        	$organ->finish_date = $faker->date($format = 'Y-m-d');
        	$organ->position = $position[$field];
        	$organ->save();
        	$counter++;
        	$field--;
        } 
        $this->command->info("Successfully created ".$counter." organizations");
    }
}

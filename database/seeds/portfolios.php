<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class portfolios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('id_ID');
		$counter = 0;
		$cover = ["","Royal-Rent-Car.png", "Airkita.png", "DSS Breas Cancer.png", "Fish-Image-Detection.png", "Automatic-Text-Summariation.png"];
		$name = ["","Royal Rent Car", "Water Condition Reporting", "Desicion Support System for Breast Cancer Diagnosing", "Fish Imaging Detection", "Automatic Text Summarization"];
		$description = ["","This is using PHP Native and MySQL", "This is using PHP Native and MySQL", "This is using Machine Learning Concept - K-Nearest Neighbor", "This is using Pattern Recognition Concep - Support Vector Machine and HOG Feature", "This using Latent Semantic Analysis"];

		$field = 1;
        for($i=1; $i<=10; $i++){
        	if($field==6) {
        		$field = 1;
        	}
        	$port = new \App\Portfolio;
        	$port->user_id = $i;
        	$port->name = $name[$field];
        	$port->description = $description[$field];
        	$port->cover = $cover[$field]; 
        	$port->save();
        	$counter++;
        	$field++;
        } 

        $field = 5;
        for($i=1; $i<=10; $i++){
        	if($field==0) {
        		$field = 5;
        	}
        	$port = new \App\Portfolio;
        	$port->user_id = $i;
        	$port->name = $name[$field];
        	$port->description = $description[$field];
        	$port->cover = $cover[$field]; 
        	$port->save();
        	$counter++;
        	$field--;
        } 
        $this->command->info("Successfully created ".$counter." portfolios");
    } 
}
